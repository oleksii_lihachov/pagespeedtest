<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

    <?php get_sidebar(); ?>

<?php endif ?>

<div id="primary" <?php astra_primary_class(); ?>>

    <?php astra_primary_content_top(); ?>
	<br>

    <?php astra_content_page_loop(); ?>

    <?php astra_primary_content_bottom(); ?>



    <div class="test" style="display: none;">

<?php

//    use GuzzleHttp\Client;
//    $client = new \GuzzleHttp\Client();
//    $response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');

//    echo $response->getStatusCode(); // 200
//    echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
//    echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'

//    // Send an asynchronous request.
//    $request = new \GuzzleHttp\Psr7\Request('GET', 'http://httpbin.org');
//    $promise = $client->sendAsync($request)->then(function ($response) {
//        echo 'I completed! ' . $response->getBody();
//    });

//    $promise->wait();

// use WidgetsBurritos\WebPageTest\WebPageTest;

// $wpt = new WebPageTest('ebfb7ff0-b2f6-41c8-bef3-4fba17be410c', $handler, 'http://18.207.160.137/');

   ?>

<?php
//
//use GuzzleHttp\Client;
//use GuzzleHttp\Exception\ClientException;
//use GuzzleHttp\Exception\RequestException;
//
///**
// * WebPageTest class.
// */
//class WebPageTest {
//
//  /**
//   * API key.
//   *
//   * @var string
//   */
//  private $apiKey;
//
//  /**
//   * Http Client.
//   *
//   * @var \GuzzleHttp\Client
//   */
//  private $client;
//
//  /**
//   * Base url.
//   *
//   * @var string
//   */
//  private $baseUrl;
//
//  /**
//   * Instantiates a new Web Page Test.
//   */
//  public function __construct($api_key, $handler = NULL, $base_url = 'http://www.webpagetest.org') {
//    $this->apiKey = $api_key;
//    $this->baseUrl = $base_url;
//    $client_options = [];
//    if (isset($handler)) {
//      $client_options['handler'] = $handler;
//    }
//    $this->client = new Client($client_options);
//  }
//
//  /**
//   * Retrieves the base url.
//   */
//  public function getBaseUrl() {
//    return $this->baseUrl;
//  }
//
//  /**
//   * Makes a get request on a url with specified query parameters.
//   */
//  private function getRequest($uri, array $query_params = [], $expect_json = TRUE) {
//    try {
//      $response = $this->client->request('GET', $uri, ['query' => $query_params]);
//    }
//    catch (ClientException $e) {
//      $response = $e->getResponse();
//    }
//    catch (RequestException $e) {
//      $response = $e->getResponse();
//    }
//
//    if ($response) {
//      $body = (string) $response->getBody();
//      if ($expect_json) {
//        return json_decode($body);
//      }
//      return $body;
//    }
//
//    return NULL;
//  }
//
//  /**
//   * Initializes a new test on the specified URL.
//   */
//  public function runTest($url, array $options = []) {
//    $uri = "{$this->baseUrl}/runtest.php";
//    $query_params = [
//      'k' => $this->apiKey,
//      'url' => $url,
//      'f' => 'json',
//    ];
//
//    return $this->getRequest($uri, $query_params + $options);
//  }
//
//  /**
//   * Retrieves the status of a test with the specified id.
//   */
//  public function getTestStatus($test_id, array $options = []) {
//    $uri = "{$this->baseUrl}/testStatus.php";
//    $query_params = [
//      'test' => $test_id,
//      'f' => 'json',
//    ];
//
//    return $this->getRequest($uri, $query_params + $options);
//  }
//
//  /**
//   * Retrieves results of test with the specified id.
//   */
//  public function getTestResults($test_id, array $options = []) {
//    $uri = "{$this->baseUrl}/jsonResult.php";
//    $query_params = [
//      'test' => $test_id,
//    ];
//
//    return $this->getRequest($uri, $query_params + $options);
//  }
//
//  /**
//   * Retrieves list of locations.
//   */
//  public function getLocations(array $options = []) {
//    $uri = "{$this->baseUrl}/getLocations.php";
//    $query_params = [
//      'f' => 'json',
//    ];
//
//    return $this->getRequest($uri, $query_params + $options);
//  }
//
//  /**
//   * Cancels a test.
//   */
//  public function cancelTest($test_id, array $options = []) {
//    $uri = "{$this->baseUrl}/cancelTest.php";
//    $query_params = [
//      'k' => $this->apiKey,
//      'test' => $test_id,
//    ];
//
//    return $this->getRequest($uri, $query_params + $options, FALSE);
//  }
//
//}
//
//$wpt = new WebPageTest('ebfb7ff0-b2f6-41c8-bef3-4fba17be410c');
//
//?>

<?php

// if ($response = $wpt->runTest('https://www.google.com')) {
//   if ($response->statusCode == StatusCode::OK) {
//     // All test info is available in $response->data.
//     $test_id = $response->data->testId;
//   }
// }
?>

		<br>

    </div>

</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

    <?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
