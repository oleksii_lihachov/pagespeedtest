<?php
/*
 * Plugin Name: Page Speed Test
 * Description: PST is a plugin providing testing tool via WebPageTest API (https://github.com/WPO-Foundation/webpagetest)
 * Author: Skynix
 * Version: 1.0
 * License: GPL
 */

global $page_speed_test;

$params = [
	"text_domain" => "websitespeedtest",
	"plugin_path" => plugin_dir_url( __FILE__ )
];

// load classes
spl_autoload_register( function ( $class_name ) {
	$classes_dir = plugin_dir_path( __FILE__ ) . '/classes/class-';
	$file        = $classes_dir . strtolower( str_replace( '_', '-', $class_name ) ) . '.php';
	if ( file_exists( $file ) ) require_once( $file );
} );


$page_speed_test = new Flow( $params );

register_activation_hook( __FILE__, function () {
	flush_rewrite_rules();
	global $page_speed_test;
	$page_speed_test->table->init();
	file_put_contents( dirname( __file__ ) . '/error_activation.txt', ob_get_contents() );
} );
register_deactivation_hook( __FILE__, function () {
	flush_rewrite_rules();
} );