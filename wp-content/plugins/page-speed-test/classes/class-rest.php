<?php

/**
 * Plugin rest api class
 */

class REST {
	private $settings;
	private $table;
	private $cron;
	private $view;
	private $rest_namespace = 'pst/v1';

	/**
	 * Constructor.
	 * @param $settings
	 * @param $table
	 * @param $cron
	 * @param $view
	 */
	public function __construct( $settings, $table, $cron, $view ) {
		$this->settings = $settings;
		$this->table    = $table;
		$this->cron     = $cron;
		$this->view     = $view;

		add_action( 'rest_api_init', array( $this, 'action_rest_api_init_trait' ) );
	}

	private function is_valid_domain( $url ) {
		$validation = FALSE;
		/*Parse URL*/
		$url_parts = parse_url( filter_var( $url, FILTER_SANITIZE_URL ) );
		/*Check host exist else path assign to host*/
		if ( ! isset( $url_parts['host'] ) ) {
			$url_parts['host'] = $url_parts['path'];
		}
		if ( $url_parts['host'] != '' ) {
			/*Add scheme if not found*/
			if ( ! isset( $url_parts['scheme'] ) ) {
				$url_parts['scheme'] = 'http';
			}
			/*Validation*/
			if ( checkdnsrr( $url_parts['host'], 'A' ) && in_array( $url_parts['scheme'], array( 'http', 'https' ) ) && ip2long( $url_parts['host'] ) === FALSE ) {
				$url_parts['host'] = preg_replace( '/^www\./', '', $url_parts['host'] );
				$url               = $url_parts['scheme'] . '://' . $url_parts['host'] . "/";
				if ( filter_var( $url, FILTER_VALIDATE_URL ) !== false && @get_headers( $url ) ) {
					$validation = TRUE;
				}
			}
		}

		return $validation;
	}

	public function action_rest_api_init_trait() {
		register_rest_route(
			$this->rest_namespace,
			'/process/',
			array(
				'methods'             => WP_REST_Server::CREATABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_process_handler' ),
			) );

		register_rest_route(
			$this->rest_namespace,
			'/test/',
			array(
				'methods'             => WP_REST_Server::READABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_test_handler' ),
			) );

		register_rest_route(
			$this->rest_namespace,
			'/update_record/',
			array(
				'methods'             => WP_REST_Server::CREATABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_update_record_front_handler' ),
			) );
	}

	public function rest_update_record_front_handler( WP_REST_Request $request ) {
		$body_json = $request->get_body();
		$body      = json_decode( $body_json, true );
		$this->table->update_status_record( $body['test_id'], $body['test_status'] );
	}

	public function rest_process_handler( WP_REST_Request $request ) {
		$settings_fields   = $this->settings->settings_fields;
		$api_token         = $settings_fields['api_token'];
		$api_url           = $settings_fields['api_url'];
		$api_token_value   = get_option( $api_token['key'] );
		$api_url_value     = get_option( $api_url['key'] );
		$requested_url     = $request->get_params()['url'];
		$requested_country = $request->get_params()['country'];

		// Validation
		if ( empty( $api_token_value ) || empty( $api_url_value ) ) {
			wp_send_json_error(
				array(
					'code'    => 'missed_options',
					'message' => 'Missed Plugins Settings'
				),
				404
			);
		}

		if ( ! empty( $api_url_value ) && filter_var( $api_url_value, FILTER_VALIDATE_URL ) === false ) {
			wp_send_json_error(
				array(
					'code'    => 'invalid_url',
					'message' => 'Api url not valid.'
				),
				404
			);
		}

		if ( empty( $requested_url ) ) {
			wp_send_json_error(
				array(
					'code'    => 'missed_options',
					'message' => 'Missed value for testing.'
				),
				404
			);
		}

		if ( ! empty( $requested_url ) ) {
			if ( $this->is_valid_domain( $requested_url ) === false ) {
				wp_send_json_error(
					array(
						'code'    => 'invalid_url',
						'message' => 'Requested url not valid.'
					),
					404
				);
			}
		}

		if ( empty( $requested_country ) ) {
			wp_send_json_error(
				array(
					'code'    => 'missed_options',
					'message' => 'Missed value for country.'
				),
				404
			);
		}

		// Send data to Testing Service
		$request_data = array(
			'url'       => $requested_url,
			'location'  => $requested_country . '.Native',
			'runs'      => '3',
			'fvonly'    => '1',
			'private'   => '1',
			'web10'     => '1',
			'f'         => 'json',
			'k'         => $api_token_value,
			'noopt'     => '1',
			'noimages'  => '1',
			'noheaders' => '1',
			'ignoreSSL' => '1',
		);
		$query        = http_build_query( $request_data );

		$ch = curl_init( $api_url_value . '?' . $query );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$curl_response = curl_exec( $ch );
		curl_close( $ch );

		if ( $curl_response === false ) {
			wp_send_json_error(
				array(
					'code'    => 'bad_request',
					'message' => 'Curl error: ' . curl_error( $ch ),
				),
				400
			);
		}

		$connect_response = json_decode( $curl_response );

		if ( $connect_response->statusCode !== 200 ) {
			$message = $connect_response->statusText ? $connect_response->statusText : 'Bad Request';
			wp_send_json_error(
				array(
					'code'    => 'bad_request',
					'message' => $message
				),
				400
			);
		}

		$test_url = $connect_response->data->jsonUrl;
		$test_id  = $connect_response->data->testId;

		if ( empty( $test_url ) ) {
			wp_send_json_error(
				array(
					'code'    => 'bad_request',
					'message' => 'Server did not return url with results'
				),
				400
			);
		}

		// Add record in Data Base
		$this->table->insert_record( $test_url, $test_id );

		// Send id to Front-End for fetching result from DB
		wp_send_json_success( array( 'id' => $test_id ) );
	}

	public function rest_test_handler( WP_REST_Request $request ) {
		$id     = $request->get_params()['id'];
		$record = $this->table->get_record( $id );
		$html   = '';

		if ( empty( $record ) ) {
			wp_send_json_error(
				array(
					'code'    => 'bad_request',
					'message' => 'Record not found in DB'
				),
				404
			);
		}

		$test_link = $record['test_link'];
		$ch        = curl_init( $test_link );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$curl_response = curl_exec( $ch );
		curl_close( $ch );
		$test_result = json_decode( $curl_response );
		$status_code = $test_result->statusCode;
		$status_text = $test_result->statusText;
		$this->table->update_status_record( $id, $status_text, (int) $status_code );

		if ( (int) $status_code === 200 ) {
			// Build html of results
			$load_time = $test_result->data->median->firstView->fullyLoaded;
			$page_size = $test_result->data->median->firstView->bytesIn;
			$requests  = $test_result->data->median->firstView->requestsFull;
			$this->table->update_record( $id, $load_time, $page_size, $requests );
			$html = $this->view->render_results( $load_time, $page_size, $requests );
		}

		wp_send_json_success(
			array(
				'status'      => $status_code,
				'status_text' => $status_text,
				'html'        => $html,
			)
		);
	}
}