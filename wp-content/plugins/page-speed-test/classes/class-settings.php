<?php
/**
 * Plugin settings class
 */

class Settings {
	public $options_page = '';
	public $text_domain = '';
	public $settings_slug = 'pst_settings';
	public $settings_fields;

	public function __construct( $args ) {
		// Set up plugins text domain
		if ( ! empty( $args["text_domain"] ) ) $this->text_domain = $args["text_domain"];

		// Set up settings fields
		$this->settings_fields = [
			"api_token" => [
				"key" => "pst_api_token",
				"label" => __( 'Api Key', $this->text_domain ),
			],
			"api_url" => [
				"key" => "pst_api_url",
				"label" => __( 'Api Live Url', $this->text_domain ),
			],
		];

		// Set where to attache settings page
		if ( ! empty( $args["options_page"] ) ) $this->options_page = $args["options_page"];
		// Add settings page
		add_action( 'admin_menu', array( $this, 'admin_page' ) );
	}


	/**
	 * Create settings page
	 */
	public function admin_page() {
		add_menu_page(
			__( 'Page Speed Test', $this->text_domain ),
			__( 'Page Speed Test', $this->text_domain ),
			'manage_options',
			$this->settings_slug,
			array( $this, 'settings_page_content' )
		);
	}


	/**
	 * Settings page content
	 */
	public function settings_page_content() {
		// check user capabilities
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}


		$api_token = $this->settings_fields["api_token"];
		$api_url   = $this->settings_fields["api_url"];

		if ( ! empty( $_POST[ $api_token['key'] ] ) ) {
			$updated = update_option( $api_token['key'], htmlentities( stripslashes( $_POST[ $api_token['key'] ] ) ) );
		}

		if ( ! empty( $_POST[ $api_url['key'] ] ) ) {
			$is_valid_url = filter_var( $_POST[ $api_url['key'] ], FILTER_VALIDATE_URL ) !== false;

			if ( $is_valid_url ) {
				$updated = update_option( $api_url['key'], htmlentities( stripslashes( $_POST[ $api_url['key'] ] ) ) );
			} else {
				echo '<div class="notice notice-error"><p>' . __( "Not valid url", $this->text_domain ) . '</p></div>';
			}
		}

		if ( ! empty( $updated ) ) {
			echo '<div class="updated notice"><p>' . __( "Settings saved", $this->text_domain ) . '</p></div>';
		}

		$api_token_value = get_option( $api_token['key'] );
		$api_url_value   = get_option( $api_url['key'] );

		?>
		<style>
			tr.ct-field th {
				padding-left: 15px;
			}

			.form-table tr.ct-field:last-child > th,
			.form-table tr.ct-field:last-child > td {
				border-bottom: none
			}

			.ct-metabox tr.ct-field {
				border-top: 1px solid #ececec
			}

			.ct-metabox tr.ct-field:first-child {
				border-top: 0
			}

			@media screen and (max-width: 782px) {
				.ct-metabox tr.ct-field > th {
					padding-top: 15px
				}

				.ct-metabox tr.ct-field > td {
					padding-bottom: 15px
				}
			}
		</style>

		<div class="wrap">
			<h1 class="wp-heading-inline"><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<form action="" method="post">
				<table class="form-table ct-metabox">
					<tr class="ct-field">
						<td>
							<label for="<?php echo $api_token['key']; ?>">
								<h3><?php echo $api_token['label']; ?>:</h3>
							</label>
							<textarea style="min-width:500px;min-height:50px;"
									  id="<?php echo $api_token['key']; ?>"
									  name="<?php echo $api_token['key']; ?>"><?php echo $api_token_value; ?></textarea>
						</td>
					</tr>
					<tr class="ct-field">
						<td>
							<label for="<?php echo $api_url['key']; ?>">
								<h3><?php echo $api_url['label']; ?>:</h3>
							</label>
							<textarea style="min-width:500px;min-height:50px;"
									  id="<?php echo $api_url['key']; ?>"
									  name="<?php echo $api_url['key']; ?>"><?php echo $api_url_value; ?></textarea>
						</td>
					</tr>
				</table>

				<?php
				// output save settings button
				submit_button( __( 'Save', $this->text_domain ) );
				?>
			</form>
		</div>
		<?php
	}
}