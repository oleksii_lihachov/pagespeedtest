<?php

/**
 * Plugin views class
 */

class View {
	private $text_domain;

	public function __construct( $text_domain ) {
		$this->text_domain = $text_domain;
	}

	public function render_results( $ms, $bytes, $requests ) {
		$html    = '';
		$seconds = number_format( $ms / 1000, 1 );
		$bytes   = number_format( $bytes / 1048576, 1 );

		$html .= '<div class="result-items">';
		$html .= '	<div class="result-items__row">';
		$html .= '		<div class="result-item">';
		$html .= '			<div class="result-item__inner">';
		$html .= '				<h4>' . __( 'Load time', $this->text_domain ) . '</h4>';
		$html .= '				<p>' . esc_html( $seconds ) . '<span>s</span></p>';
		$html .= '			</div>';
		$html .= '		</div>';
		$html .= '		<div class="result-item">';
		$html .= '			<div class="result-item__inner">';
		$html .= '				<h4>' . __( 'Page size', $this->text_domain ) . '</h4>';
		$html .= '				<p>' . esc_html( $bytes ) . '<span>MB</span></p>';
		$html .= '			</div>';
		$html .= '		</div>';
		$html .= '		<div class="result-item">';
		$html .= '			<div class="result-item__inner">';
		$html .= '				<h4>' . __( 'Requests', $this->text_domain ) . '</h4>';
		$html .= '				<p>' . esc_html( $requests ) . '</p>';
		$html .= '			</div>';
		$html .= '		</div>';
		$html .= '	</div>';
		$html .= '</div>';

		return $html;
	}
}