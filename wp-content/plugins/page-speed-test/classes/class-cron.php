<?php

/**
 * Plugin crones class
 */

class Cron {
	public $table;

	public function __construct( $table ) {
		$this->table = $table;

		add_action( 'pst_cron_hook', array( $this, 'check_service' ), 10, 2 );
		add_filter( 'cron_schedules', array( $this, 'custom_interval' ) );
	}

	public function custom_interval() {
		$recurrence['10_sec'] = array(
			'interval' => 10,
			'display'  => 'Every 10 seconds',
		);
		return $recurrence;
	}

	public function add( $arg ) {
		if ( ! wp_next_scheduled( 'pst_cron_hook', $arg ) ) {
			wp_schedule_event(
				time(),
				'10_sec',
				'pst_cron_hook',
				$arg
			);
		}
	}

	public function check_service( $id, $url ) {
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$curl_response = curl_exec( $ch );
		curl_close( $ch );
		$test_result = json_decode( $curl_response );
		$status_code = $test_result->statusCode;
		$status_text = $test_result->statusText;

		$this->table->update_status_record( $id, $status_text, (int) $status_code );

		if ( $status_code === 400 ) {
			wp_clear_scheduled_hook( 'pst_cron_hook', array( $id, $url ) );
		}

		if ( $status_code === 200 ) {
			wp_clear_scheduled_hook( 'pst_cron_hook', array( $id, $url ) );
			$load_time = $test_result->data->median->firstView->fullyLoaded;
			$page_size = $test_result->data->median->firstView->bytesIn;
			$requests  = $test_result->data->median->firstView->requestsFull;
			$this->table->update_record( $id, $load_time, $page_size, $requests);
		}
	}
}
