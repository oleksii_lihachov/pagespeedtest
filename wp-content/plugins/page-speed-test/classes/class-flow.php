<?php

/**
 * Main plugin class
 */

/**
 * Class Flow
 */
class Flow {
	public $text_domain = '';
	public $plugin_path;
	private $settings;
	public $table;
	public $cron;
	public $view;

	/**
	 * Constructor.
	 * @param $args
	 */
	public function __construct( $args ) {
		// Set up plugins text domain
		if ( ! empty( $args["text_domain"] ) ) $this->text_domain = $args["text_domain"];
		// Set plugin path
		if ( ! empty( $args["plugin_path"] ) ) $this->plugin_path = $args["plugin_path"];

		// Set up settings class
		$this->settings = new Settings( $args );
		// Database table class
		$this->table = new Table();
		// Cron class
		$this->cron = new Cron( $this->table );
		// View components
		$this->view = new View( $this->text_domain );
		// Add Rest routes
		new Rest( $this->settings, $this->table, $this->cron, $this->view );

		add_action( 'wp_enqueue_scripts', array( $this, 'init_front_assets' ) );
	}

	public function init_front_assets() {
		wp_enqueue_style(
			'pst-styles',
			$this->plugin_path . '/dist/style.css',
			array(),
			null
		);

		wp_enqueue_script(
			'pst-scripts',
			$this->plugin_path . '/dist/bundle.js',
			array( 'jquery' ),
			null,
			true
		);

		// Add ajax-url to main script
		wp_localize_script( 'pst-scripts',
			'user_info', array(
				'ajax_url' => admin_url( 'admin-ajax.php' )
			) );
	}
}
