<?php

/**
 * Class Table
 */
class Table {
	private $wpdb;
	private $table_name;

	public function __construct() {
		global $wpdb;
		$this->wpdb       = &$wpdb;
		$this->table_name = $wpdb->prefix . "pst_cron_list";
	}

	public function init() {
		$charset_collate = $this->wpdb->get_charset_collate();

		// Check if table already exist
		$search_table_query = $this->wpdb->prepare( 'SHOW TABLES LIKE %s', $this->wpdb->esc_like( $this->table_name ) );
		if ( $this->wpdb->get_var( $search_table_query ) === $this->table_name ) {
			return false;
		}

		$sql = "CREATE TABLE {$this->table_name} (
				  `cron_id` INT NOT NULL AUTO_INCREMENT,
				  `test_id` VARCHAR(255) NOT NULL,
  				 `cron_status` VARCHAR(45) NULL,
  				 `status_text` VARCHAR(255) NULL,
  				 `status_code` INT NULL,
  				 `test_link` VARCHAR(255) NULL,
  				 `cron_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  				 `load_time` INT NULL,
  				 `page_size` INT NULL,
  				 `requests` INT NULL,
  				 UNIQUE INDEX `cron_id_UNIQUE` (`cron_id` ASC),
  				 PRIMARY KEY (`cron_id`)) {$charset_collate};";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		return true;
	}

	public function insert_record( $test_link, $test_id ) {
		$this->wpdb->insert(
			$this->table_name,
			array(
				'test_id'     => $test_id,
				'test_link'   => $test_link,
				'cron_date'   => date( 'Y-m-d H:i:s' )
			)
		);
	}

	public function update_status_record( $id, $status_text, $status_code = '' ) {
		$this->wpdb->update(
			$this->table_name,
			array(
				'status_text' => $status_text,
				'status_code' => $status_code,
			),
			array(
				'test_id' => $id,
			)
		);
	}

	public function update_record( $id, $load_time, $page_size, $requests ) {
		$this->wpdb->update(
			$this->table_name,
			array(
				'load_time'   => $load_time,
				'page_size'   => $page_size,
				'requests'    => $requests,
			),
			array(
				'test_id' => $id,
			)
		);
	}

	public function get_record( $id ) {
		return $this->wpdb->get_row( "SELECT * FROM {$this->table_name} WHERE test_id = '{$id}'", ARRAY_A );
	}
}