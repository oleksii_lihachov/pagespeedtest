import originalFetch from 'isomorphic-fetch';
import fetchRetry from 'fetch-retry';
import {throttle} from 'lodash';

const fetch = fetchRetry(originalFetch);

export default function speedForm() {
	const wrapper = document.querySelector('.speed-form-wrapper');
	const form = document.querySelector('#speed-form');
	const resultsBox = document.querySelector('.js-hook__results');
	let timerId = null;
	let controller = null;
	let testId = null;

	function startProcess() {
		wrapper.classList.add('active');
	}

	function endProcess() {
		wrapper.classList.remove('active');
	}

	function updateUi(resultHtml = '') {
		resultsBox.innerHTML = resultHtml;
	}

	function updateRecordStatus(id, status) {
		let data = {
			test_id: id,
			test_status: status
		};

		fetch(
			`${window.location.href}wp-json/pst/v1/update_record/`,
			{
				method: 'POST',
				body: JSON.stringify(data),
				retries: 10,
				retryDelay: 10000
			});
	}

	window.onbeforeunload = function () {
		if (testId && timerId) {
			updateRecordStatus(testId, 'User leave a page');
		}
	};

	function getRecord(id) {
		controller = new AbortController();
		const {signal} = controller;

		fetch(
			`${window.location.href}wp-json/pst/v1/test/?id=${id}`,
			{
				retries: 60,
				retryDelay: 10000,
				signal
			})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				const {success, data} = response;

				if (success) {
					const {status, status_text, html} = data;

					if (+status === 200) {
						clearTimeout(timerId);
						updateUi(html);
						endProcess();
					} else {
						updateUi(status_text);
						timerId = setTimeout(getRecord, 10000, id);
					}
				} else {
					throw data;
				}
			})
			.catch(function (error) {
				endProcess();
				updateUi(`<p><strong>${error.code}</strong> ${error.message}</p>`);
				updateRecordStatus(id, error.message);
			});
	}

	if (typeof (form) != 'undefined' && form != null) {
		const setupProcess = throttle(function () {
			startProcess();
			updateUi();

			fetch(
				`${window.location.href}wp-json/pst/v1/process/`,
				{
					method: 'POST',
					body: new FormData(form),
					retries: 60,
					retryDelay: 10000
				}
			).then(function (response) {
				return response.json();
			}).then(function (response) {
				const {success, data} = response;

				if (success) {
					if (timerId) {
						clearTimeout(timerId);
					}

					if (controller && testId) {
						updateRecordStatus(testId, 'Request abort via Front-end');
						controller.abort();
						controller = null;
					}

					testId = data.id;
					getRecord(data.id);
				} else {
					throw data;
				}
			}).catch(function (error) {
				endProcess();
				updateUi(`<p><strong>${error.code}</strong> ${error.message}</p>`);
			});
		}, 3000);


		form.addEventListener('submit', function (ev) {
			ev.preventDefault();
			setupProcess();
		});
	}
}