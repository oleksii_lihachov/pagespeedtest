# README #

### All logic of Web Page test API in page-speed-test plugin
After Activation 
***
Need insert **Api Key** and **Api Url** fields 
https://monosnap.com/file/4RlesVbwSmiuALQf7jF96O5tUUvJi0
***
### Frontend Assets
`wp-content/themes/astra/assets/js/modules/speed-form.js`
***
`wp-content/themes/astra/assets/scss/components/speed-form.scss`
***
### Building front-end
In theme uses `webpack`
***
run `npm i` in `wp-content/themes/astra` for installing dependencies
***
run `npm start` for building un minified versions with source maps
***
run `npm run build` for building production version with 
***
### UI of section is
#### form 

```
<div class="speed-form-wrapper">
	<form id="speed-form" class="speed-form" action="/" method="post">
		<div class="speed-form__inner">
			<div class="speed-form__col speed-form__col--first">
				<input class="speed-form__input" maxlength="50" name="url" required="" type="text" placeholder="Enter the Website URL">
			</div>
			<div class="speed-form__col speed-form__col--second">
				<div class="speed-form-select">
					<select name="country">
						<option value="ap-south-1" selected="">India (Mumbai)</option>
						<option value="us-east-2">USA (Ohio)</option>
						<option value="sa-east-1">Germany (Frankfurt)</option>
						<option value="ap-southeast-1">Singapore</option>
						<option value="sa-east-1">Brazil (Sao Paulo)</option>
						<option value="ap-southeast-2">Australia (Sydney)</option>
					</select>
					<svg xmlns="http://www.w3.org/2000/svg" class="speed-form-select__icon" viewBox="0 0 42.242 23.949">
						<path fill="#868e96" stroke="#868e96" stroke-width="4" d="M2.828 1.414l18.293 18.293L39.414 1.414l1.414 1.414-19.707 19.707L1.414 2.828z"></path>
					</svg>
				</div>
			</div>
			<div class="speed-form__col speed-form__col--third">
				<button class="speed-form__submit" type="submit">test speed</button>
			</div>
		</div>
	</form>
	<div class="speed-form-loader">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="speed-form-info">
		<p class="p">Please take a coffee break, your test result will be ready soon.</p>
		<p class="p">We are running <strong>9 tests</strong> at the same time, to find the most reliable loading time of your website.</p>
		<p class="p">The required testing time will vary based on, how much time your website takes to load (Speed of your website).</p>
	</div>
	<div class="js-hook__results"></div>
</div>
```
